<?php

namespace App\Lib;

use App\Lib\Render\Parameters;

class TextParser
{
    protected $inputString;

    protected $outputString;

    // @todo private $foreach

    public function __construct(string $inputString)
    {
        $this->inputString = $inputString;
        $this->outputString = $inputString;
    }

    /**
     * @return int[]
     */
    protected function findLineStartPositions(array $positions): array
    {
        $output = [];

        foreach ($positions as $position) {
            $output[] = $this->findLineStartPosition($position);
        }

        return $output;
    }

    /**
     * @param int $position
     * @return int|null
     */
    private function findLineStartPosition(int $position): ?int
    {
        // Get string chunk before
        $stringBefore = substr($this->outputString, 0, $position);
        // Get all linebreak positions
        $linebreakPositions = $this->findAllSubstringPositions("\n", $stringBefore);
        // Take last linebreak position
        return count($linebreakPositions) === 0 ? null : end($linebreakPositions) + 1; // "\n" is 1 chars long
    }

    /**
     * @return int[]
     */
    protected function findAllSubstringPositions(string $needle, string $input = null): array
    {
        $input = $input ?? $this->outputString;
        $lastPos = 0;
        $positions = [];

        while (($lastPos = strpos($input, $needle, $lastPos)) !== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($needle);
        }

        return $positions;
    }

    protected function findFirstSubstringPosition(string $needle): int
    {
        return strpos($this->outputString, $needle);
    }

    protected function getTextChunks(array $positions): array
    {
        $output = [];
        $chunksPositions = [];

        for ($i = 0; $i < count($positions); $i++) {
            $nextIndex = $i + 1;
            $nextPosition = @$positions[$nextIndex];
            $chunkEndPosition = $nextPosition - 1;

            if ($nextIndex > count($positions) - 1) {
                $chunkEndPosition = strlen($this->outputString);
            }

            $chunksPositions[] = [$positions[$i], $chunkEndPosition];
        }

        foreach ($chunksPositions as $chunkPositions) {
            list($fromPosition, $toPosition) = $chunkPositions;

            $output[] = substr($this->outputString, $fromPosition, $toPosition - $fromPosition);
        }

        return $output;
    }

    /**
     * @return string[]
     */
    protected function initTextChunkObjects(array $textChunks, string $objectClassTitle): array
    {
        return array_map(function ($textChunk) use ($objectClassTitle) {
            return new $objectClassTitle($textChunk);
        }, $textChunks);
    }

    final public function render(Parameters $parameters = null)
    {
        return new Render($this, $parameters);
    }

    final public function getOutputString()
    {
        return $this->outputString;
    }

    // @todo NB!!! NO NEED Should be only in main parser (IssueParser) -> use interface NB!!! NO NEED
    public function renderCustom(): string
    {
        return $this->outputString;
    }

    // @todo Move to Interface
    public function fetchData(){

    }

}