<?php

namespace App\Lib;

use App\Lib\Render\Parameters;

final class Render
{
    /** @var TextParser */
    private $parser;

    /** @var Parameters */
    private $parameters;

    public function __construct(TextParser $parser, Parameters $parameters = null)
    {
        $this->parser = $parser;
        $this->parameters = $parameters ?? new Parameters();
    }

    private function render(string $string)
    {
        return $this->parameters->getAddBefore() . $string . $this->parameters->getAddAfter() ;
    }

    public function custom()
    {
        return $this->parser->renderCustom();
    }

    public function firstLine()
    {
        return $this->render(explode("\n", $this->parser->getOutputString())[0]);
    }

    public function all()
    {
        return $this->render($this->parser->getOutputString());
    }
}