<?php

namespace App\Lib\Gitlab\Issue;

use App\Lib\Gitlab\Issue\Parser\Section;
use App\Lib\Gitlab\Issue\Parser\Task;
use App\Lib\Render\Parameters;
use App\Lib\TextParser;

class Parser extends TextParser
{
    /** @var Section[] */
    private array $sections;

    /** @var string[] */
    private array $sectionHeaderNeedles = [
        'UNSORTED',
        '\*****',
        '\****',
        '***',
        '**',
        '*',
        '0 *',
        'COMPLETED'
    ];

    private string $sectionHeaderSprintf = "# %s\n";

    /**
     * @var Task[]|array
     */
    private array $completedTasks = [];


    public function __construct(string $inputString)
    {
        parent::__construct($inputString);
        $this->process();
    }

    private function process()
    {
        $this->sections = $this->parseSections();

        // @todo $this->foreach($this->getSections(), function(Section $section){
        //  ...
        // });

        foreach ($this->getSections() as $section) {

            if (!$section->isEmpty()) {

                $this->completedTasks = array_merge($this->completedTasks, $section->extractCompletedTasks());

                /** @var Task $task */
                foreach ($section->getTasks() as $task) {
                    $task->addCreationTimestampIfNeeded();
                }

                foreach ($this->completedTasks as $task) {
                    $task->addCreationTimestampIfNeeded();
                    $task->addCompletionTimestampIfNeeded();
                }
            }
        }
    }

    public function renderCustom(): string
    {
        $output = '';

        foreach ($this->getSections() as $section) {
            // $output .= $section?->isFirstInGroup()->render() PHP 8.0 required
            //$output .= $section->isFirstInLoop() ? $section->render()->firstLine() : null;
            
            $output .= $section // @todo ->isFirstInGroup()->render()
            ->render((new Parameters())->setAddBefore("\n")->setAddAfter("\n"))
                ->firstLine();
            foreach ($section->getTasks() as $task) {
                $output .= $task->render((new Parameters())->setAddBefore("\n"))->all();
            }
        }

        /** @var Task $completedTask */
        foreach ($this->completedTasks as $completedTask) {
            $output .= $completedTask->render((new Parameters())->setAddAfter("\n"))->all();
        }

        return $output;
    }

    /**
     * @return Section[]
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @return Section[]
     */
    private function parseSections(): array
    {
        return $this->initTextChunkObjects(
            $this->getTextChunks(
                $this->findLineStartPositions(
                    $this->findHeaderPositions($this->sectionHeaderNeedles)
                )
            ),
            Section::class
        );
    }

    /**
     * @return int[]
     */
    private function findHeaderPositions(array $needles): array
    {
        $output = [];

        for ($i = 0; $i < count($needles); $i++) {
            $output[] = $this->findFirstSubstringPosition(sprintf($this->sectionHeaderSprintf, $needles[$i]));
        }
        return $output;
    }
}