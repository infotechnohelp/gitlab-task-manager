<?php

namespace App\Lib\Gitlab\Issue\Parser;

use App\Lib\TextParser;

class Section extends TextParser
{
    /** @var Task[] */
    private $tasks;

    public function __construct(string $inputString)
    {
        parent::__construct($inputString);
        $this->tasks = $this->parseTasks();
    }

    /**
     * @return Task[]
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }

    /**
     * @return Task[]
     */
    private function parseTasks(): array
    {
        $taskLineStartPositions = $this->findLineStartPositions(array_merge(
            $this->findAllSubstringPositions('- [x]'),
            $this->findAllSubstringPositions('- [ ]')
        ));

        sort($taskLineStartPositions);

        return $this->initTextChunkObjects($this->getTextChunks($taskLineStartPositions), Task::class);
    }

    public function extractCompletedTasks(): array
    {
        $completedTasks = [];

        array_walk($this->tasks, function (Task $task, int $key) use (&$completedTasks) {
            if ($task->isCompleted()) {
                $completedTasks[] = $task;
                unset($this->tasks[$key]);
            }
        });

        return $completedTasks;
    }

    public function isEmpty()
    {
        return count($this->getTasks()) === 0;
    }
}