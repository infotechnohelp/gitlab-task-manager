<?php

namespace App\Lib\Gitlab\Issue\Parser;

use App\Lib\TextParser;

class Task extends TextParser
{
    private $completedTaskCheckboxNeedle = '- [x]';
    private $openedTaskCheckboxNeedle = '- [ ]';
    private $timestampFormat = "d.m.y H:i";
    private $timestampLength = 14 + 22; // <sub><sup>15.12.95 12:00</sup></sub>


    public function isCompleted(): bool
    {
        return strpos($this->outputString, $this->completedTaskCheckboxNeedle);
    }

    public function creationTimestampExists(): bool
    {
        $checkboxNeedle = $this->isCompleted() ? $this->completedTaskCheckboxNeedle : $this->openedTaskCheckboxNeedle;
        $timestampString = substr(
            $this->outputString,
            $this->findFirstSubstringPosition($checkboxNeedle) + strlen($checkboxNeedle) + 1, // space
            $this->timestampLength);

        // @todo DRY: Move to parameters 10 & 14
        return \DateTime::createFromFormat($this->timestampFormat, substr($timestampString, 10, 14)) !== false;
    }

    public function completionTimestampExists(): bool
    {
        $checkboxNeedle = $this->isCompleted() ? $this->completedTaskCheckboxNeedle : $this->openedTaskCheckboxNeedle;
        $timestampString = substr(
            $this->outputString,
            $this->findFirstSubstringPosition($checkboxNeedle) + strlen($checkboxNeedle) + 1 // space
                   + $this->timestampLength + 3, // ' - '
            $this->timestampLength); // second date substring

        return \DateTime::createFromFormat($this->timestampFormat, substr($timestampString, 10, 14)) !== false;
    }


    public function addCreationTimestampIfNeeded()
    {
        if (!$this->creationTimestampExists()) {
            $checkboxNeedle = $this->isCompleted() ? $this->completedTaskCheckboxNeedle : $this->openedTaskCheckboxNeedle;
            $creationTimestampEndPosition = $this->findFirstSubstringPosition($checkboxNeedle) + strlen($checkboxNeedle);

            $this->outputString = substr_replace(
                $this->outputString,
                " <sub><sup>" . (new \DateTime())->format($this->timestampFormat) . "</sup></sub>" . substr($this->outputString, $creationTimestampEndPosition),
                $creationTimestampEndPosition
            );
        }
    }

    public function addCompletionTimestampIfNeeded()
    {
        if (!$this->completionTimestampExists()) {
            $checkboxNeedle = $this->isCompleted() ? $this->completedTaskCheckboxNeedle : $this->openedTaskCheckboxNeedle;
            $completionTimestampStartPosition = $this->findFirstSubstringPosition($checkboxNeedle) + strlen($checkboxNeedle)
                + $this->timestampLength + 1;
            $this->outputString = substr_replace(
                $this->outputString,
                " - <sub><sup>" . (new \DateTime())->format($this->timestampFormat) . "</sup></sub>" . substr($this->outputString, $completionTimestampStartPosition),
                $completionTimestampStartPosition
            );
        }
    }

}