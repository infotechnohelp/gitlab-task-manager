<?php

namespace App\Lib\Render;

final class Parameters
{
    /**
     * @var string|null
     */
    private ?string $addBefore = null;

    /**
     * @var string|null
     */
    private ?string $addAfter = null;

    /**
     * @var string|null
     */
    private ?string $removeBefore = null;

    /**
     * @var string|null
     */
    private ?string $removeAfter = null;

    /**
     * @var string|null
     */
    private ?string $mustBeBefore = null;

    /**
     * @var string|null
     */
    private ?string $mustBeAfter = null;

    /**
     * @return string|null
     */
    public function getAddBefore(): ?string
    {
        return $this->addBefore;
    }

    /**
     * @param string|null $addBefore
     * @return Parameters
     */
    public function setAddBefore(?string $addBefore): Parameters
    {
        $this->addBefore = $addBefore;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddAfter(): ?string
    {
        return $this->addAfter;
    }

    /**
     * @param string|null $addAfter
     * @return Parameters
     */
    public function setAddAfter(?string $addAfter): Parameters
    {
        $this->addAfter = $addAfter;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRemoveBefore(): ?string
    {
        return $this->removeBefore;
    }

    /**
     * @param string|null $removeBefore
     * @return Parameters
     */
    public function setRemoveBefore(?string $removeBefore): Parameters
    {
        $this->removeBefore = $removeBefore;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRemoveAfter(): ?string
    {
        return $this->removeAfter;
    }

    /**
     * @param string|null $removeAfter
     * @return Parameters
     */
    public function setRemoveAfter(?string $removeAfter): Parameters
    {
        $this->removeAfter = $removeAfter;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMustBeBefore(): ?string
    {
        return $this->mustBeBefore;
    }

    /**
     * @param string|null $mustBeBefore
     * @return Parameters
     */
    public function setMustBeBefore(?string $mustBeBefore): Parameters
    {
        $this->mustBeBefore = $mustBeBefore;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMustBeAfter(): ?string
    {
        return $this->mustBeAfter;
    }

    /**
     * @param string|null $mustBeAfter
     * @return Parameters
     */
    public function setMustBeAfter(?string $mustBeAfter): Parameters
    {
        $this->mustBeAfter = $mustBeAfter;
        return $this;
    }


}